# CyzaTestAksenchyk

This project was generated with [angular-cli](https://github.com/angular/angular-cli) version 1.0.0-beta.19-3.

## Development server
Run dev server:

```bash

npm run dev

```

If you have docker installed:


```bash

npm run dockerize

```

after:

```bash

docker-compose up

```

To run tests: 

```bash

npm run test

```