import { LoginPage } from './login.po';
import { urlChanged } from './helpers';
import { browser, element, by } from 'protractor';
import { getPath } from './helpers';

describe('Login page tests', function () {
    let page: LoginPage;

    beforeEach(() => {
        page = new LoginPage();
    });

    it('should set login form and fill inputs with default values', () => {
        browser.get('/');
        expect(page.getLoginHeadText()).toEqual('Login');
        expect(element(by.css('input[type="password"]'))
            .getAttribute('value'))
            .toEqual('password');
    });

    it('should redirect to /auth', () => {
        browser.get('/');
        expect(getPath()).toEqual('http://localhost:4200/#/auth');
    });

    it('should go to profile after successfull login', () => {
        element(by.css('input[type="submit"]')).click();
        browser.wait(urlChanged("http://localhost:4200/#/profile/settings"), 5000);
    })
});