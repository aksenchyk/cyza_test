import { browser, element, by, $ } from 'protractor';

export class ProfilePage {

    urlChanged(url) {
        return function () {
            return browser.getCurrentUrl().then(function (actualUrl) {
                return url != actualUrl;
            });
        };
    };
    getInfoContainer() {
        return element($('div.info-container'));
    }
    getHeaderBox() {
        return element($('div.header-box'));
    }

    getLayoutContainer() {
        return element($('div.profile-layout-container'));
    }
}
