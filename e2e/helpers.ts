import { browser, element, by } from 'protractor';
export function urlChanged(url) {
    return function () {
        return browser.getCurrentUrl().then(function (actualUrl) {
            return url != actualUrl;
        });
    };
};

export function getPath() {
    return browser.getCurrentUrl();
};

export function setWindowSize(w, h) {
    browser.driver.manage().window().setSize(w, h);
}

export function maximize() {
    browser.driver.manage().window().maximize();
}

export function setPosition(x, y) {
    browser.driver.manage().window().setPosition(x, y);
}
