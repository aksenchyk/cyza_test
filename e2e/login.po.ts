import { browser, element, by, $ } from 'protractor';

export class LoginPage {
  getLoginHeadText() {
    return element(by.css('.head')).getText();
  }
}
