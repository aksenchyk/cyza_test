import { ProfilePage } from './profile.po';
import { urlChanged, maximize, setWindowSize } from './helpers'
import { browser, element, by, protractor, $ } from 'protractor';

describe('Profile page tests', function () {
    let page: ProfilePage;
    var EC = protractor.ExpectedConditions;
    beforeEach(() => {
        page = new ProfilePage();
    });

    it('should redirect to settings by default', () => {
        browser.get('/profile');
        browser.wait(page.urlChanged("http://localhost:4200/#/profile/settings"), 5000);
    });

    it('should render profile info', () => {
        browser.get('/auth');
        element(by.css('input[type="submit"]')).click();
        browser.wait(EC.presenceOf($('div.profile-layout-container')), 5000);
        var container = $('div.profile-layout-container');
        expect(container.element(by.css('h4')).getText()).toBe('First Last');
    })

    it('should render layout properly', () => {
        browser.get('/auth');
        element(by.css('input[type="submit"]')).click();
        browser.wait(EC.presenceOf($('div.profile-layout-container')), 5000);
        var container = $('div.profile-layout-container');
        maximize();
        expect(container.getCssValue('padding-left')).toBe('15px');
        setWindowSize(400, 600);
        expect(container.getCssValue('padding-left')).toBe('0px');
    });
});