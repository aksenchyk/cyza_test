import { browser, element, by } from 'protractor';

describe('App basic tests', function () {
  it('should set app title', () => {
    browser.get('/');
    expect(browser.getTitle()).toBe('aksenchyk.v@gmail.com');
  });
});
