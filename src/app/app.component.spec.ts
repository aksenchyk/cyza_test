/* tslint:disable:no-unused-variable */

import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { SharedModule } from './shared';
import { APP_IMPORTS } from './app.imports';
import { NotFound404Component } from './404Not_found.component';
import { RouterTestingModule } from '@angular/router/testing';
import { CORE_SERVICES } from './core/services';
import { AppConfig } from './app.config';

const PROVIDERS = [
  ...CORE_SERVICES
];

describe('App: CyzaTestAksenchyk', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        NotFound404Component
      ],
      imports: [
        SharedModule,
        RouterTestingModule
      ],
      providers: [
        ...PROVIDERS
      ]
    });
  });

  it('should create the app', async(() => {
    let fixture = TestBed.createComponent(AppComponent);
    let app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  it('should set app configuration', async(() => {
    let fixture = TestBed.createComponent(AppComponent);   
    expect(AppConfig.getApiVersion()).toBe('api/v1');
  }));
});
