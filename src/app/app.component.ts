import { Component } from '@angular/core';
import { AppConfig } from './app.config';
import { environment } from '../environments/environment';
import { AuthService } from './core/services'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor(private auth: AuthService) {
    if (environment.production) {
      AppConfig.setBaseURL('http://cyzainc.aksenchyk.com:4042/');
    } else {
      AppConfig.setBaseURL('http://localhost:4042/');
    }
    auth.init();
  }
}
