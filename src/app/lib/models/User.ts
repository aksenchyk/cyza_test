export interface UserInterface {
    //password?: string;
    //email?: string;

    refreshToken?: string;
    accessToken?: any;

    isAuthenticated?: () => boolean;
}

export class User implements UserInterface {


    //password?: string;
    //email?: string;


    accessToken?: string;
    refreshToken?: string;
    constructor(data?: any) {
        Object.assign(this, data);
    }

    isAuthenticated(): boolean {
        return !!this.accessToken;
    }
}