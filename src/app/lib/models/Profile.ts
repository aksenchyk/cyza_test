/* tslint:disable */

export interface ProfileInterface {
  address?: any;
  email?: string;
  firstName?: string;
  lastName?: string;
  phone?: string;
  reviewsCount?: number;
  stars?: number;
  website?: string;
  followersCount?: number;
}

export class Profile implements ProfileInterface {
  address: any;
  email: string;
  firstName: string;
  lastName: string;
  phone: string;
  reviewsCount: number;
  stars: number;
  website: string;
  followersCount?: number;
  constructor(instance?: Profile) {
    Object.assign(this, instance);
  }
}
