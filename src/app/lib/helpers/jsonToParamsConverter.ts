import { Injectable } from '@angular/core';
import { URLSearchParams } from '@angular/http';

export class JSONToParamsConverter {
    private static _usp: URLSearchParams;

    static getURLSearchParams(obj: any): URLSearchParams {
        return new URLSearchParams(JSONToParamsConverter._JSON2URL(obj, false));
    }

    private static _JSON2URL(obj: any, parent: any) {
        var parts: any = [];
        for (var key in obj)
            parts.push(JSONToParamsConverter._parseParam(key, obj[key], parent));
        return parts.join('&');
    }

    private static _parseParam(key: string, value: any, parent: string) {
        if (typeof value !== 'object' && typeof value !== 'array') {
            return parent ? parent + '[' + key + ']=' + value
                : key + '=' + value;
        } else if (typeof value === 'object' || typeof value === 'array') {
            return parent ? JSONToParamsConverter._JSON2URL(value, parent + '[' + key + ']')
                : JSONToParamsConverter._JSON2URL(value, key);
        } else {
            throw new Error('Unexpected Type');
        }
    }
}
