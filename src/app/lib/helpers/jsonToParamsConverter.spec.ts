import { JSONToParamsConverter } from './jsonToParamsConverter';

describe('JSON CONVERTER TESTS TESTS', () => {
    it('should convert properly', () => {
        const stringParam = JSONToParamsConverter.getURLSearchParams({ first: 'first', second: 'second' });
        expect(stringParam.toString()).toBe('first=first&second=second')
    });
})

