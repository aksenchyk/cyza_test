/* tslint:disable: variable-name */
import { Routes } from '@angular/router';
import { ProfileBaseComponent } from './profileBase.component';
import { Profile_AboutComponent } from './components/about.component';
import { Profile_SettingsComponent } from './components/settings.component';

export const routes: Routes = [
  {
    path: '',
    component: ProfileBaseComponent,
    children: [
      { path: 'about', component: Profile_AboutComponent },
      { path: 'settings', component: Profile_SettingsComponent },
      { path: '', redirectTo: 'about', pathMatch: 'full' }
    ]
  }
];