import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ProfileApi } from '../../../shared/services';

@Component({
  selector: 'profile-settings',
  templateUrl: './settings.component.html'
})

export class Profile_SettingsComponent {
  changePasswordForm: FormGroup;
  model: any = {
    oldPassword: 'password',
    newPassword: '',
    confirmpassword: ''

  }
  error: string;
  message: string;
  submitted: boolean = false;
  constructor(private profileApi: ProfileApi) {
    this.changePasswordForm = new FormGroup({
      oldpassword: new FormControl('password', Validators.minLength(2)),
      newpassword: new FormControl('', Validators.minLength(2)),
      confirmpassword: new FormControl('', Validators.minLength(2)),
    }, (group: FormGroup) => group.get('newpassword').value === group.get('confirmpassword').value
      ? null : { 'equal': true });
  }

  onSubmit(form: FormGroup) {
    this.submitted = true;
    if (form.valid)
      this.profileApi.changepasswordPassword({
        oldPassword: form.value['oldpassword'],
        newPassword: form.value['newpassword']
      })
        .subscribe(
        (data) => this.onSuccess(data),
        (err) => this.onError(err));
    else {
      this.error = 'Form is not valid';
    }
  }
  onSuccess(data) {
    this.changePasswordForm.reset();
    for (const key in this.model) {
      this.model[key] = '';
    }
    this.message = 'Changed sucessfully'
  }
  onError(err) {
    this.error = 'Password change failed';
  }
}
