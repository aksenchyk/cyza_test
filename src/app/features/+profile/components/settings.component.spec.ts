/* tslint:disable */
import { ApplicationRef, Component } from '@angular/core';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';

import { By } from '@angular/platform-browser';
import { ProfileApi } from '../../../shared/services';
import { AuthService } from '../../../core/services';
import { Router, RouterModule, ActivatedRoute } from '@angular/router';
import { Subject, Observable } from 'rxjs';
import { TestBed, async, inject, ComponentFixture, fakeAsync, tick } from '@angular/core/testing'
import { SharedModule } from '../../../shared';
import { RatingModule } from 'ng2-bootstrap/ng2-bootstrap';
import { BehaviorSubject } from 'rxjs'

import { Profile_SettingsComponent } from './settings.component';
import { RouterTestingModule } from '@angular/router/testing';

let fixture: ComponentFixture<Profile_SettingsComponent>,
    comp: Profile_SettingsComponent,
    profileApiService: ProfileApi, mockedRouter: Router;

describe('COMPONENTS TESTS', () => {   
    //-------- Mock classes and test variables 
    class MockProfileApiService {
        public getProfile() {
            return Observable.throw({});
        }
        public changepasswordPassword() { }
        changed$: BehaviorSubject<any> = new BehaviorSubject({ accessToken: 'token' });
    }

    class MockAuthService { }
    //-------------------------

    describe('PROFILE COMPONENTS TESTS', () => {
        beforeEach(() => {
            TestBed.configureTestingModule({
                declarations: [Profile_SettingsComponent],
                imports: [
                    SharedModule
                ],
                providers: [
                    { provide: ProfileApi, useValue: new MockProfileApiService() },
                    { provide: AuthService, useClass: MockAuthService },
                ]
            });
            fixture = TestBed.createComponent(Profile_SettingsComponent);
            TestBed.compileComponents();
        });

        it('should allow api call only vhen form is valid', fakeAsync(() => {
            const profileApiINst = fixture.debugElement.injector.get(ProfileApi);
            spyOn(profileApiINst, 'changepasswordPassword').and.returnValue(Observable.of({}));
            let instance: Profile_SettingsComponent = fixture.debugElement.componentInstance;
            instance.model.oldpassword = 'old';
            instance.model.newpassword = 'new';
            instance.model.confirmpassword = 'new';
            fixture.detectChanges();
            instance.onSubmit(instance.changePasswordForm)
            tick();
            expect(profileApiINst.changepasswordPassword).toHaveBeenCalled();
        }));
        it('should stop api call if form is not valid', fakeAsync(() => {
            const profileApiINst = fixture.debugElement.injector.get(ProfileApi);
            spyOn(profileApiINst, 'changepasswordPassword').and.returnValue(Observable.of({}));
            let instance: Profile_SettingsComponent = fixture.debugElement.componentInstance;
            instance.model.oldpassword = 'old';
            instance.model.newpassword = 'new';
            instance.model.confirmpassword = 'notnew';
            fixture.detectChanges();
            instance.onSubmit(instance.changePasswordForm)
            tick();
            expect(profileApiINst.changepasswordPassword).not.toHaveBeenCalled();
        }));
    })
})

