import { Component } from '@angular/core';
import { ProfileApi } from '../../../shared/services';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'profile-about',
  templateUrl: './about.component.html',
  styleUrls: [
    './about.component.scss'
  ]
})
export class Profile_AboutComponent {
  error;
  message;
  // variable to save temp state. Always represents backend value.
  formSnapshot: any = {
    address: {
     
    }
  };
  firstName: string;
  lastName: string;
  website: string;
  phone: string;
  address: any;
  max: number = 5;
  private _stars;
  set stars(value) {
    this._stars = value;
    //  this.save(true); backend doesn't change value;
  };
  get stars() {
    return this._stars;
  }

  profileForm: FormGroup;
  constructor(private profileApi: ProfileApi) {
    this.profileForm = new FormGroup({
      firstName: new FormControl('password'),
      lastName: new FormControl(''),
      website: new FormControl(''),
      address: new FormControl({}),
      stars: new FormControl(0),
      phone: new FormControl('')
    });
  }
  ngOnInit() {
    this.profileApi.getProfile()
      .subscribe((profileData) => {
        if (profileData) {
          this.firstName = profileData.firstName;
          this.lastName = profileData.lastName;
          this.phone = profileData.phone;
          this.address = profileData.address;
          this.stars = profileData.stars;
          this.website = profileData.website
          this.formSnapshot = {
            firstName: profileData.firstName,
            lastName: profileData.lastName,
            phone: profileData.phone,
            stars: profileData.stars,
            address: profileData.address,
            website: profileData.website
          };
        }
      });
  }
  cancelForm() {
    this.phone = this.formSnapshot.phone;
    this.website = this.formSnapshot.website;
    this.address = this.formSnapshot.address;
    this.firstName = this.formSnapshot.firstName;
    this.lastName = this.formSnapshot.lastName;
    this.stars = this.formSnapshot.stars;
  }

  save(ignoreValidation?: boolean) {
    if (this.profileForm.valid || ignoreValidation)
      this.profileApi.patchofCreateProfile({
        name: this.firstName,
        firstName: this.firstName,
        lastName: this.lastName,
        website: this.website,
        address: this.address,
        stars: this.stars,
        phone: this.phone
      }).subscribe(() => {
        this.formSnapshot = {
          firstName: this.firstName,
          lastName: this.lastName,
          phone: this.phone,
          stars: this.stars,
          address: this.address,
          website: this.website
        };
        // notify profileBase that data changed
        this.profileApi.notify(this.formSnapshot);
        this.message = 'Sucessfully updated';
      }, (err) => {
        this.error = 'Some with backend error occured';
      });
  }
}
