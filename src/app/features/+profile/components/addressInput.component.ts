import {
    Component, Input, QueryList, AfterContentInit, ViewChild,
    ViewContainerRef, ElementRef, ContentChildren, Renderer, HostListener
} from '@angular/core';

import {
    Output, OnInit, Optional, Host,
    EventEmitter, forwardRef
} from '@angular/core';
import {
    FormGroup,
    Validators,
    FormBuilder,
    NG_VALIDATORS, FormControl, NgForm
} from '@angular/forms';
import {
    NgControl, NgModel,
    ControlValueAccessor, NG_VALUE_ACCESSOR
} from '@angular/forms';

import { getDOM, DomAdapter } from '@angular/platform-browser/src/dom/dom_adapter';

class Address {
    city?: string;
    state?: string;
    zip?: string;
}
/**
 * Custom input component for adress field  
 */
@Component({
    selector: 'address-input',
    template: `                   
                
                <div [class.hidden]="!isPopupMode" class="material-dropdown" [class.active]='!isHidden'>                    
                    <a  (click)='isHidden=!isHidden' [class.active]='isHidden'  class="material-dropdown_btn">                      
                        <ng-content></ng-content>
                    </a>               
                </div> 
                <!--styled according to mode (default or popup). Popup is for mobile. -->
              <!--  <div [class.hidden]="isHidden"  [style.paddingLeft.px]="isPopupMode ? 20 : 0">
                    <form role="form" id="login-form" [formGroup]="addressForm" no-validate>
                        <div class="form-group" [style.marginTop.px]="!isPopupMode ? 15 : 35">
                            <input type="text" class="form-control" name="city" class="form-control" id="city"  [(ngModel)]='addrValue.city' formControlName="city"/>
                            <label class="float-label" for="username">CITY</label>
                        </div>
                        <div class="form-group">
                            <input class="form-control" type="text" name="state" class="form-control" class="form-control"  [(ngModel)]='addrValue.state' id="state"
                                formControlName="state" />
                            <label class="float-label" for="state">STATE</label>
                        </div>
                        <div class="form-group">
                            <input class="form-control" type="text" name="zip" class="form-control" class="form-control" [(ngModel)]='addrValue.zip'  id="zip"
                                formControlName="zip"  />
                            <label class="float-label" for="password">ZIP</label>
                        </div>
                    </form>    
                </div> --> <!-- end of container block -->  
                  <div [class.hidden]="isHidden"  [style.paddingLeft.px]="isPopupMode ? 20 : 0">
                    <form role="form" #heroForm="ngForm" id="login-form"  no-validate>
                        <div class="form-group" [style.marginTop.px]="!isPopupMode ? 15 : 35">
                            <input type="text" class="form-control" name="city" class="form-control" id="city"  [(ngModel)]='addrValue.city' />
                            <label class="float-label" for="username">CITY</label>
                        </div>
                        <div class="form-group">
                            <input class="form-control" type="text" name="state" class="form-control" class="form-control"  [(ngModel)]='addrValue.state' id="state"
                                 />
                            <label class="float-label" for="state">STATE</label>
                        </div>
                        <div class="form-group">
                            <input class="form-control" type="text" name="zip" class="form-control" class="form-control" [(ngModel)]='addrValue.zip'  id="zip"
                                 />
                            <label class="float-label" for="password">ZIP</label>
                        </div>
                    </form>    
                </div>             
        
    `,
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => AddressInputComponent),
            multi: true
        }
    ]
})
export class AddressInputComponent implements ControlValueAccessor {
    //addressForm: FormGroup;
    @ViewChild('heroForm') addressForm: NgForm;
    private _addrValue: Address = {

    }
    set addrValue(value) { this._addrValue = value; };
    get addrValue(): Address { return this._addrValue }
    isPopupMode = false;
    isHidden = false;

    @Input('mode') set mode(value) {
        if (value == 'popup') {
            this.isPopupMode = true;
            this.isHidden = true;
        }
    };
    ngAfterViewInit() {
        this.addressForm.form.valueChanges
            .distinctUntilChanged()
            .filter((value: Address) => value.city != undefined && value.state != undefined && value.zip != undefined)
            .subscribe((address) => {
                this.addressChange.next(address);
            });
    }

    onTouched = () => {
    };
    propagateChange = (_: any) => { };

    @Output()
    addressChange: EventEmitter<any> = new EventEmitter();
    writeValue(value: Address) {
        if (value && value.city !== undefined) {
            this.addrValue.city = value.city;
        }
        if (value && value.state !== undefined) {
            this.addrValue.state = value.state;
        }
        if (value && value.zip !== undefined) {
            this.addrValue.zip = value.zip;
        }
    }
    registerOnChange(fn): void {
        this.addressChange.subscribe(fn);
    }
    registerOnTouched(fn): void {
        this.onTouched = fn;
    }

}
