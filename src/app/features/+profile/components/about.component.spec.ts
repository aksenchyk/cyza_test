/* tslint:disable */
import { ApplicationRef, Component } from '@angular/core';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';

import { By } from '@angular/platform-browser';
import { ProfileApi } from '../../../shared/services';
import { AuthService } from '../../../core/services';
import { Router, RouterModule, ActivatedRoute } from '@angular/router';
import { Subject, Observable } from 'rxjs';
import { TestBed, async, inject, ComponentFixture, fakeAsync, tick } from '@angular/core/testing'
import { SharedModule } from '../../../shared';
import { RatingModule } from 'ng2-bootstrap/ng2-bootstrap';
import { BehaviorSubject } from 'rxjs'

import { Profile_AboutComponent } from './about.component';
import { AddressInputComponent } from './addressInput.component';
import { RouterTestingModule } from '@angular/router/testing';

let fixture: ComponentFixture<Profile_AboutComponent>,
    comp: Profile_AboutComponent,
    profileApiService: ProfileApi, mockedRouter: Router;

describe('COMPONENTS TESTS', () => {

    //-------- Mock classes and test variables    

    const profile = {
        firstName: "firstname",
        lastName: 'lastname',
        website: 'website',
        address: {
            city: 'address',
            state: 'State',
            zip: 'ZIP'
        },
        phone: 'phone'
    }
    class MockProfileApiService {
        public getProfile() {
            return Observable.of(profile);
        }
        public patchofCreateProfile() {
            return Observable.of({})
        }
        public notify(data) { }
        changed$: BehaviorSubject<any> = new BehaviorSubject({ accessToken: 'token' });
    }
    class MockAuthService { }

    //------------------------------------

    describe('PROFILEBASE COMPONENT TESTS', () => {
        beforeEach(() => {
            // test component config
            TestBed.configureTestingModule({
                declarations: [Profile_AboutComponent, AddressInputComponent],
                imports: [
                    SharedModule, RatingModule
                ],
                providers: [
                    { provide: ProfileApi, useValue: new MockProfileApiService() },
                    { provide: AuthService, useClass: MockAuthService },
                ]
            });
            fixture = TestBed.createComponent(Profile_AboutComponent);
            //TestBed.compileComponents();
        });

        it('should render component', async(() => {
            let instance = fixture.debugElement.componentInstance;
            expect(instance).toBeTruthy();
        }));
        it('should set profile data into fields', async(() => {
            
            fixture.whenStable().then(() => {
                let compiled = fixture.debugElement.nativeElement;
                fixture.detectChanges();
                expect(compiled.querySelector('input#firstName').value).toContain('firstname');
            });

        }));

        xit('the same is for mobile-only version (check span)', async(() => {
            let compiled = fixture.debugElement.nativeElement;
            fixture.whenStable().then(() => {
                fixture.detectChanges();
                expect(compiled.querySelector('#firstNameGroup').textContent).toContain('firstname');
            });
        }));

        xit('should reset to snapshot object when cancel the form', fakeAsync(() => {
            let instance: Profile_AboutComponent = fixture.debugElement.componentInstance;
            let compiled = fixture.debugElement.nativeElement;
            tick();
            fixture.detectChanges();
            instance.firstName = 'firstname_new';
            fixture.detectChanges();
            expect(compiled.querySelector('input#firstName').value).toBe('firstname_new');
            instance.cancelForm();
            fixture.detectChanges();
            expect(compiled.querySelector('input#firstName').value).toBe('firstname');
        }));

        xit('should call profile api when save', fakeAsync(() => {
            let instance: Profile_AboutComponent = fixture.debugElement.componentInstance;
            let compiled = fixture.debugElement.nativeElement;
            fixture.detectChanges();
            let apiService = fixture.debugElement.injector.get(ProfileApi);
            spyOn(apiService, 'notify').and.callFake(() => { });
            instance.lastName = 'lastName_new'; // it've been changed by user           
            fixture.detectChanges();
            instance.save(true);
            tick();
            expect(compiled.querySelector('input#lastName').value).toBe('lastName_new');
            expect(instance.formSnapshot.lastName).toBe('lastName_new');
            expect(apiService.notify).toHaveBeenCalled();
            tick();
        }));
    })
})

