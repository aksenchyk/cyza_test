import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { routes } from './profile.routing';

import { ProfileBaseComponent } from './profileBase.component';
import { Profile_AboutComponent } from './components/about.component';
import { Profile_SettingsComponent } from './components/settings.component';
import { AddressInputComponent } from './components/addressInput.component';
import { RatingModule } from 'ng2-bootstrap/ng2-bootstrap';
import { SharedModule } from '../../shared'

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    RatingModule,
    SharedModule
  ],
  declarations: [
    ProfileBaseComponent,
    Profile_AboutComponent,
    Profile_SettingsComponent,
    AddressInputComponent
  ]
})

export class ProfileModule {}