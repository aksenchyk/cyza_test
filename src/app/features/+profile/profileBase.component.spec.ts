/* tslint:disable */
import { ApplicationRef, Component } from '@angular/core';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';

import { By } from '@angular/platform-browser';
import { ProfileBaseComponent } from "./profileBase.component";
import { ProfileApi } from '../../shared/services';
import { AuthService } from '../../core/services';
import { Router, RouterModule, ActivatedRoute } from '@angular/router';
import { Subject, Observable } from 'rxjs';
import { TestBed, async, inject, ComponentFixture, fakeAsync, tick } from '@angular/core/testing'
import { SharedModule } from '../../shared';
import { RatingModule } from 'ng2-bootstrap/ng2-bootstrap';
import { BehaviorSubject } from 'rxjs'

import { Profile_AboutComponent } from './components/about.component';
import { Profile_SettingsComponent } from './components/settings.component';
import { AddressInputComponent } from './components/addressInput.component';
import { RouterTestingModule } from '@angular/router/testing';

import { routes } from './profile.routing';

let fixture: ComponentFixture<ProfileBaseComponent>,
    comp: ProfileBaseComponent,
    profileApiService: ProfileApi, mockedRouter: Router;

describe('COMPONENTS TESTS', () => {
    class MockProfileApiService {
        public getProfile() {
            return Observable.throw({});
        }
        changed$: BehaviorSubject<any> = new BehaviorSubject({ accessToken: 'token' });
    }

    class MockAuthService { }

    describe('PROFILEBASE COMPONENT TESTS', () => {
        beforeEach(() => {
            TestBed.configureTestingModule({
                declarations: [ProfileBaseComponent, Profile_AboutComponent, Profile_SettingsComponent, AddressInputComponent],
                imports: [
                    SharedModule, RatingModule, RouterTestingModule
                ],
                providers: [
                    { provide: ProfileApi, useValue: new MockProfileApiService() },
                    { provide: AuthService, useClass: MockAuthService },
                ]
            });
        });

        it('should navigate back to login page if user tokens are invalid', fakeAsync(() => {
            fixture = TestBed.createComponent(ProfileBaseComponent);
            let instance: ProfileBaseComponent = fixture.debugElement.componentInstance;           
            spyOn(instance, 'redirectToAuth').and.callFake(function () {});
            fixture.detectChanges();
            TestBed.compileComponents();
            tick();
            fixture.detectChanges();
            expect(instance.redirectToAuth).toHaveBeenCalled();
        }));
    })
})

