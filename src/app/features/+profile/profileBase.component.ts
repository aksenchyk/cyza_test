import { Component, ViewEncapsulation } from '@angular/core';
import { UserApi, ProfileApi } from '../../shared/services';
import { Router } from '@angular/router';
import { Profile } from '../../lib/models';

@Component({
  selector: 'my-profile',
  templateUrl: './profileBase.component.html',
  styleUrls: ['./profileBase.component.scss']
})

export class ProfileBaseComponent {
  profile: Profile;
  constructor(private userApi: UserApi, private profileApi: ProfileApi, private router: Router) { }

  ngOnInit() {
    this.profileApi
      .changed$
      .subscribe((profileData) => {
        this._getProfile();
      })
    this._getProfile();
  }
  private _getProfile() {
    this.profileApi.getProfile()
      .subscribe((profileData) => {
        this.profile = new Profile(profileData);
      }, (err) => {
        this.redirectToAuth();
      });
  }
  redirectToAuth() {
    this.router.navigate(['/auth']);
  }
  onLogout() {
    this.userApi.logout();
    this.redirectToAuth();
  }
}
