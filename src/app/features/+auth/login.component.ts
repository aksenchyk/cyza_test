import { Component } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { UserApi } from '../../shared/services';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs'

@Component({
    selector: 'my-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent {
    signInForm: FormGroup;
    error: string;
    authSubscr: Subscription;
    constructor(builder: FormBuilder,
        private router: Router,
        private userApi: UserApi       
    ) {
        this.signInForm = builder.group({
            'username': ['email@example.com'],
            'password': ['password']
        });
    }  
    onSubmit(value) {        
        this.userApi.login(value)
            .subscribe(
            (data) => this.onSuccess(data),
            (err) => this.onError(err));
    }
    onSuccess(data) {
        this.router.navigate(['/profile']);       
    }
    onError(err) {
        this.error = 'Login failed';
    }
}