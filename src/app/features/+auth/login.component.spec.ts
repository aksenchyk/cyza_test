/* tslint:disable */
import { ApplicationRef, Component } from '@angular/core';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';

import { By } from '@angular/platform-browser';
import { LoginComponent } from "./login.component";
import { UserApi } from '../../shared/services';
import { AuthService } from '../../core/services';
import { Router, RouterModule } from '@angular/router';
import { Subject, Observable } from 'rxjs';
import { TestBed, async, inject, ComponentFixture, fakeAsync, tick } from '@angular/core/testing'
import { SharedModule } from '../../shared';

let fixture: ComponentFixture<LoginComponent>,
    comp: LoginComponent,
    userApiService: UserApi, mockedRouter: Router;

describe('COMPONENTS TESTS', () => {
    class MockUserApiService {
        public login(credentials: any, include: any = 'user') {
            return Observable.of({});
        }
    }
    class MockRouter {
        navigate(value) { }
    }

    class MockAuthService {
    }

    describe('LOGIN COMPONENT TESTS', () => {
        beforeEach(() => {
            TestBed.configureTestingModule({
                declarations: [LoginComponent],
                imports: [
                    SharedModule
                ],
                providers: [
                    { provide: UserApi, useValue: new MockUserApiService() },
                    { provide: Router, useClass: MockRouter },
                    { provide: AuthService, useClass: MockAuthService },
                ]
            })
            fixture = TestBed.createComponent(LoginComponent);
            fixture.detectChanges();
            TestBed.compileComponents();
        });

        beforeEach(async(() => {
            /** Spies, Mocks */
            userApiService = fixture.debugElement.injector.get(UserApi);
            mockedRouter = fixture.debugElement.injector.get(Router);
            spyOn(userApiService, 'login').and.returnValue(Observable.of({ result: 'ok' }));
            spyOn(mockedRouter, 'navigate');
        }));

        it('should render component and set default values', async(() => {
            let compiled = fixture.debugElement.nativeElement;
            expect(compiled.querySelector('.head').textContent).toContain('Login');
            expect(compiled.querySelector('#username').value).toBe('email@example.com');
            expect(compiled.querySelector('#password').value).toBe('password');
        }));

        it('shold show error alert if componnet has login problems', async(() => {
            let compiled = fixture.debugElement.nativeElement;
            let instance: LoginComponent = fixture.debugElement.componentInstance;
            instance.error = 'Some error';
            fixture.detectChanges();
            expect(compiled.querySelector('.material-alert_danger').textContent).toContain('Some error');
        }));

        it('should call call auth service when user click login button', fakeAsync(() => {
            let compiled = fixture.debugElement.nativeElement;
            let instance: LoginComponent = fixture.debugElement.componentInstance;
            const value = { 'username': 'username' }
            instance.onSubmit(value)
            fixture.detectChanges();
            tick();
            fixture.detectChanges();
            expect(userApiService.login).toHaveBeenCalledWith(value);
        }));
        it('should redirect to pfofile after successful login', fakeAsync(() => {
            let compiled = fixture.debugElement.nativeElement;
            let instance: LoginComponent = fixture.debugElement.componentInstance;
            instance.onSubmit({})
            fixture.detectChanges();
            tick();
            fixture.detectChanges();
            expect(mockedRouter.navigate).toHaveBeenCalledWith(['/profile']);
        }));
    })
})

