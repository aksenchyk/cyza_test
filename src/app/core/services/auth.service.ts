import { Injectable } from '@angular/core';
import { StorageDriver } from '../../lib/helpers';
import { Subject, Observable } from 'rxjs';
import { User } from '../../lib/models';

@Injectable()
export class AuthService {
  private _user: User;

  protected propsPrefix: string = '$Cyza$';
  private _dispatch: Subject<User> = new Subject<User>();
  constructor() {
    this._user = new User();
  }

  public get identity$(): Observable<User> {
    return this._dispatch.asObservable();
  };

  public getCurrentUser() {
    return this._user;
  }

  public setUser(data) {
    if (!!data && !!data.accessToken) {
      this._user.accessToken = data.accessToken;
      this._user.refreshToken = data.refresh_token;
    } else {
      this._user.accessToken = this._user.refreshToken = null;
    }   
    this.saveToStorage("accessToken", this._user.accessToken);
    this.saveToStorage("refreshToken", this._user.refreshToken);
    this._dispatch.next(this._user);
  }

  init() {
    const user = this.initFromStorage();
    this._user = user;
  }

  public initFromStorage(): User {
    var accessToken = this.load('accessToken');
    var refreshToken = this.load('refreshToken');
    var user = new User();
    user.accessToken = this.load('accessToken');
    user.refreshToken = this.load('refreshToken');
    return user;
  }

  public clearStorage() {
    StorageDriver.remove(this.propsPrefix + 'accessToken');
    StorageDriver.remove(this.propsPrefix + 'refreshToken');
  };

  protected saveToStorage(name: string, value: any) {
    try {
      var key = this.propsPrefix + name;
      StorageDriver.set(key, value);
    }
    catch (err) {
      console.log('Cannot access local/session storage:', err);
    }
  }

  protected load(name: string): any {
    var key = this.propsPrefix + name;
    return StorageDriver.get(key);
  }
}

 