import { AuthService } from './auth.service';
export * from './auth.service';

/* one instance only services */
export const CORE_SERVICES = [
    AuthService
] 