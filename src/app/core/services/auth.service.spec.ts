import {

    async,
    fakeAsync
} from '@angular/core/testing';

import { BaseRequestOptions, Http } from '@angular/http';
import { Router } from '@angular/router';
import { MockBackend } from '@angular/http/testing';

import { Observable, ReplaySubject } from "rxjs";
import { AuthService } from "./auth.service";
import { StorageDriver } from '../../lib/helpers';

var fakeUser = { token: 'fakeToken', name: 'john' };
describe('Auth service tests', () => {
    let service: AuthService;
    beforeEach(() => {
        StorageDriver.get = function (key) {
            const storage = {
                $Cyza$accessToken: 'accessToken',
                $Cyza$refreshToken: 'refreshToken'
            }
            return storage[key];
        }
        
    });
    beforeEach(() => {
        service = new AuthService();
        service.init(); //called whe app starts
    })

    it('should init user from storage', () => {
        expect(service.getCurrentUser().accessToken).toBe('accessToken');
        expect(service.getCurrentUser().refreshToken).toBe('refreshToken');
    });

    it('should set new user and dispatch with user data', () => {
        service.identity$.subscribe(user => {
            expect(user.accessToken).toEqual('newaccessToken');          
        })
        service.setUser({ accessToken: 'newaccessToken' })
    })
});

