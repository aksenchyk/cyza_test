import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';

import { AppComponent } from './app.component';
import { SharedModule } from './shared';
import { APP_IMPORTS } from './app.imports';
import { NotFound404Component } from './404Not_found.component';
import { CORE_SERVICES } from './core/services';

const PROVIDERS = [
  ...CORE_SERVICES
];

@NgModule({
  declarations: [
    AppComponent,
    NotFound404Component
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    SharedModule,
    APP_IMPORTS
  ],
  providers: [
    ...PROVIDERS,
    { provide: LocationStrategy, useClass: HashLocationStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor() {

  }
}
