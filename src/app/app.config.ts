export class AppConfig {
    private static path: string = 'http://0.0.0.0:4042/';
    private static version: string = 'api/v1';
    private static authPrefix: string = '';
    private static debug: boolean = true;
    private static clientId = 'a823jkas87y3kjakjhsd';
    private static clientSecret = 'dksu287aokjfaouiusdia7127a5skd';

    public static setApiVersion(version: string = 'api'): void {
        AppConfig.version = version;
    }

    public static getApiVersion(): string | number {
        return AppConfig.version;
    }

    public static setBaseURL(url: string = '/'): void {
        AppConfig.path = url;
    }

    public static getPath(): string {
        return AppConfig.path;
    }

    public static getSecret(): string {
        return AppConfig.clientSecret;
    }

    public static getId(): string {
        return AppConfig.clientId;
    }

    public static setAuthPrefix(authPrefix: string = ''): void {
        AppConfig.authPrefix = authPrefix;
    }

    public static getAuthPrefix(): string {
        return AppConfig.authPrefix;
    }

}