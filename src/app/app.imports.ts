import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule, PreloadAllModules } from '@angular/router';

import { routes } from './app.routing';

export const APP_IMPORTS = [ 
  RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })  
];