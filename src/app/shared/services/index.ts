import { ProfileApi } from './profile.service';
import { ErrorHandler } from './error.service';
import { UserApi } from './user.service';
import { HttpBase } from './httpBase.service';


export * from './profile.service';
export * from './error.service';
export * from './user.service';
export * from './httpBase.service';

export var SHARED_SERVICES: Array<any> = [
   ProfileApi, ErrorHandler, UserApi
]
