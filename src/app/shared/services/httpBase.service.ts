import { Injectable, Inject, Optional } from '@angular/core';
import { Http, Headers, Request } from '@angular/http';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

import { Observable, Observer } from 'rxjs';

import {
  ErrorHandler
} from './error.service';

import {
  AuthService
} from '../../core/services';

import {
  JSONToParamsConverter
} from '../../lib/helpers';

import { AppConfig } from '../../app.config';

/**
 * Base class for all http-based services.
 */
export abstract class HttpBase {

  protected path: string;
  constructor(protected http: Http, protected auth: AuthService, protected errorHandler: ErrorHandler) { }
   /**
    * Process request over http
    * @param string  method      Request method (GET, POST, PUT, PATCH)
    * @param string  url         Request url (my-host/my-url/:id)
    * @param any     routeParams Values of url parameters
    * @param any     urlParams   Parameters for building url (filter and other)
    * @param any     postBody    Request postBody
    * @param boolean protect     Should attach authorization headers
    */
  public request(
    method: string,
    url: string,
    routeParams: any = {},
    urlParams: any = {},
    postBody: any = null,
    protect: boolean = true
  ) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    const user = this.auth.getCurrentUser();

    if (protect && user && user.accessToken) {
      headers.append(
        'authorization',
        'Bearer ' + user.accessToken
      );
    }
    let requestUrl = url;
    let key: string;
    for (key in routeParams) {
      requestUrl = requestUrl.replace(
        new RegExp(":" + key + "(\/|$)", "g"),
        routeParams[key] + "$1"
      );
    }
    let body: any;
    if (
      typeof postBody === 'object' &&
      (postBody.data || postBody.credentials || postBody.options) &&
      Object.keys(postBody).length === 1
    ) {
      body = postBody.data ? postBody.data :
        postBody.options ? postBody.options :
          postBody.credentials;
    } else {
      body = postBody;
    }
    let request: Request = new Request({
      headers: headers,
      method: method,
      url: requestUrl,
      search: JSONToParamsConverter.getURLSearchParams(urlParams),
      body: body ? JSON.stringify(body) : undefined
    });
    return this.http.request(request)
      .map(res => (res.text() != "" ? res.json() : {}))
      .catch(this.errorHandler.handleServerError);
  }
}
