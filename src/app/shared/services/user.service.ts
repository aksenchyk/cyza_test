
/* tslint:disable */
import { Injectable, Inject, Optional } from '@angular/core';
import { Http, Response } from '@angular/http';
import { AppConfig } from '../../app.config';
import {
  HttpBase
} from './httpBase.service';
import {
  AuthService
} from '../../core/services';
import {
  ErrorHandler
} from './error.service';

import { User } from '../../lib/models';


/**
 * Api services for the `User` model.
 */
@Injectable()
export class UserApi extends HttpBase {

  constructor(
    @Inject(Http) http: Http,
    @Inject(AuthService) protected auth: AuthService,
    @Optional() @Inject(ErrorHandler) errorHandler: ErrorHandler
  ) {
    super(http, auth, errorHandler);
  }

  /**
   * Login a user with test email and password. 
   */

  public login(credentials: any) {
    let method: string = "POST";
    let url: string = AppConfig.getPath() + AppConfig.getApiVersion() +
      "/auth/token";
    let routeParams: any = {};
    let postBody: any = {};
    let urlParams: any = {
      client_id: AppConfig.getId(),
      client_secret: AppConfig.getSecret(),
      username: credentials.username,
      password: credentials.password,
      grant_type: 'password'
    };

    let result = this
      .request(method, url, routeParams, urlParams, postBody, false)
      .do((response) => {
        this.auth.setUser({ accessToken: response.access_token, refresh_token: response.refresh_token });
      });
    return result;
  }

  public logout() {
    // revoke token if needed
    this.auth.setUser(null);
  }
}
