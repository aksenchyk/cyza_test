import { UserApi } from './user.service';
import { ErrorHandler } from './error.service';
import { AuthService } from '../../core/services'
import { User } from '../../lib/models';
import { Injector } from '@angular/core';
import { BaseRequestOptions, Http, Response, ResponseOptions } from '@angular/http';
import { MockBackend, MockConnection } from '@angular/http/testing';

import { TestBed, async, inject, ComponentFixture, fakeAsync, tick } from '@angular/core/testing'

var userApiSrvs: UserApi;
let userData: any = { accessToken: 'token', refreshToken: 'rtoken' };

let mockBackend: MockBackend;

class MockAuthService {
    setUser() {
    }
    getCurrentUser() {
        return new User({ accessToken: 'token', refreshToken: 'refToken' })
    }
}

describe('User service Tests', function () {
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                UserApi,
                ErrorHandler,
                MockBackend,
                BaseRequestOptions,
                {
                    provide: Http,
                    useFactory: (backend: MockBackend, options: BaseRequestOptions) => new Http(backend, options),
                    deps: [MockBackend, BaseRequestOptions]
                },
                {
                    provide: AuthService,
                    useClass: MockAuthService
                }
            ]
        });
    });

    beforeEach(inject([MockBackend, UserApi], (_mockBackend_: MockBackend, _userApi_: UserApi) => {
        mockBackend = _mockBackend_;
        userApiSrvs = _userApi_;
    }));


    it('should fetch the user acess tokens`', (done: () => void) => {
        mockBackend.connections.subscribe((conn: MockConnection) => {
            //conn.request.headers
            conn.mockRespond(new Response(new ResponseOptions({ body: JSON.stringify(userData) })));
        });
        userApiSrvs.login({ username: 'username', password: 'password' }).subscribe(result => {
            expect(result).toEqual(userData);
            done();
        });
    });
})
