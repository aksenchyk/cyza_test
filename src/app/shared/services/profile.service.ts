/* tslint:disable */
import { Injectable, Inject, Optional } from '@angular/core';
import { Http, Response } from '@angular/http';
import { AppConfig } from '../../app.config';
import {
  HttpBase
} from './httpBase.service';
import {
  AuthService
} from '../../core/services';
import {
  ErrorHandler
} from './error.service';

import { Profile } from '../../lib/models';
import { Subject, Observable } from 'rxjs';

/**
 * Api services for the `Profile` model.
 */
@Injectable()
export class ProfileApi extends HttpBase {
  private _dispatch: Subject<Profile> = new Subject<Profile>();
  constructor(
    @Inject(Http) http: Http,
    @Inject(AuthService) protected auth: AuthService,
    @Optional() @Inject(ErrorHandler) errorHandler: ErrorHandler
  ) {
    super(http, auth, errorHandler);
  }
  public get changed$(): Observable<Profile> {
    return this._dispatch.asObservable();
  };
  public notify(profile){
    this._dispatch.next(profile);
  }

  public getProfile() {
    let method: string = "GET";
    let url: string = AppConfig.getPath() + AppConfig.getApiVersion() +
      "/profile";
    let result = this.request(method, url, {}, {}, {});
    return result;
  }

  public changepasswordPassword(data: any = undefined) {
    let method: string = "PUT";
    let url: string = AppConfig.getPath() + AppConfig.getApiVersion() +
      "/profile/changepassword";
    let routeParams: any = {};
    let postBody: any = data;
    let urlParams: any = {};
    let result = this.request(method, url, routeParams, urlParams, postBody);
    return result;
  }

  public patchofCreateProfile(data: any = undefined) {
    let method: string = "PATCH";
    let url: string = AppConfig.getPath() + AppConfig.getApiVersion() +
      "/profile";
    let routeParams: any = {};
    let postBody: any = {
      data: data
    };
    let urlParams: any = {};
    let result = this.request(method, url, routeParams, urlParams, postBody);
    return result;
  }
}