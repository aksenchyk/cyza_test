/* tslint:disable */
import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/throw';
/**
 * Default error handler
 */
@Injectable()
export class ErrorHandler {
  public handleServerError(error: Response) {
    return Observable.throw(error.json().error || 'Server error');
  }
  public handleAuthError(error: any) {
    return Observable.throw('Login failed');
  }
}