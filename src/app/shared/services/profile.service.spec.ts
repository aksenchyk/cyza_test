import { ProfileApi } from './profile.service';
import { ErrorHandler } from './error.service';
import { AuthService } from '../../core/services'
import { User } from '../../lib/models';
import { Injector } from '@angular/core';
import { BaseRequestOptions, Http, Response, ResponseOptions } from '@angular/http';
import { MockBackend, MockConnection } from '@angular/http/testing';
import { TestBed, async, inject, ComponentFixture, fakeAsync, tick } from '@angular/core/testing';
var profileApiSrvs: ProfileApi;
let userData: any = { accessToken: 'token', refreshToken: 'rtoken' };
let mockBackend: MockBackend;

class MockAuthService {
    setUser() {
    }
    getCurrentUser() {
        return new User({ accessToken: 'token', refreshToken: 'refToken' })
    }
}

describe('User service Tests', function () {
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                ProfileApi,
                ErrorHandler,
                MockBackend,
                BaseRequestOptions,
                {
                    provide: Http,
                    useFactory: (backend: MockBackend, options: BaseRequestOptions) => new Http(backend, options),
                    deps: [MockBackend, BaseRequestOptions]
                },
                {
                    provide: AuthService,
                    useClass: MockAuthService
                }
            ]
        });
    });

    beforeEach(inject([MockBackend, ProfileApi], (_mockBackend_: MockBackend, _profileApi_: ProfileApi) => {
        mockBackend = _mockBackend_;
        profileApiSrvs = _profileApi_;
    }));

    it('should fetch the user profile and set secure headers`', (done: () => void) => {
        mockBackend.connections.subscribe((conn: MockConnection) => {
            expect(conn.request.headers.get('authorization')).toBe('Bearer token');
            conn.mockRespond(new Response(new ResponseOptions({ body: JSON.stringify(userData) })));
        });
        profileApiSrvs.getProfile().subscribe(result => {
            expect(result).toEqual(userData);
            done();
        });
    });
})
