import { Directive, Input, Output, EventEmitter } from '@angular/core';
@Directive({
    selector: '[href]',
    host: {
        '(click)': 'preventDefault($event)'
    }
})
export class InertLink {
    @Input() href;
    preventDefault(event) {
        if (this.href.length === 0) event.preventDefault();
    }
}