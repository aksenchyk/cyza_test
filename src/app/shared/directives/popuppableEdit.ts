import {
    Component, Input, ViewChild,
    ElementRef, ContentChildren, Renderer, HostListener
} from '@angular/core';

import {
    Output, OnInit, Optional, Host,
    EventEmitter
} from '@angular/core';


import { getDOM, DomAdapter } from '@angular/platform-browser/src/dom/dom_adapter';

@Component({
    selector: 'myPopup',
    exportAs: 'popup',
    template: `
    <div #popupContainer class='popup-container' [class.hidden]='isHidden'>
     <div class="row">
         <div class="col-md-12 col-sm-12 col-lg-12">        
             <ng-content></ng-content>
             <div class='buttons-block'>
                <button type="button" class='btn material-btn material-btn-primery' (click)='save($event)'>Save</button>
                <button type="button" class='btn material-btn material-btn-primery' (click)='cancel($event)'>Cancel</button>
             </div>           
         </div>       
     </div>
     <div>
    `,
    styles: [
        `
        .hidden { display:none }
        .popup-container{ z-index:999; position: absolute; background: white; padding:20px; max-width: 300px; box-shadow: 0px 1px 15px 10px rgba(0, 0, 0, 0.05), 0 1px 6px 0px rgba(0,0,0,0.13); }
        .popup-container:before{           
            content: "";
            border-style: solid;
            border-width: 10px 10px 10px 0;
            border-color: transparent #ffffff transparent transparent;
            position: absolute;
            left: -10px;
            top: 10px;
        }
        .buttons-block{ margin-top: 20px;}
        .buttons-block > button {
            margin-right: 20px;
        }
        .header{ font-size: 0.8em; color: #bac1c7 }
        .form-group {
            margin-top: 5px!important;
            margin-bottom: 10px!important;
        }
        `
    ]
})

export class PopupComponent {
    @Input('linkTo') link: string;
    @Input('control') control: string;

    @ViewChild('popupContainer') container: ElementRef;
    target: HTMLElement;
    private _doc: HTMLDocument;
    private _domAdapter: DomAdapter;
    constructor(private renderer: Renderer, private _elementRef: ElementRef) { }
    isHidden: boolean = true;

    ngAfterViewInit() {
        this._domAdapter = getDOM();
        this._doc = this._domAdapter.defaultDoc();
        if (this.link) {
            this.target = this._doc.getElementById(this.link);
        }
    }

    @HostListener("document:click", ['$event.target'])
    public onClick(targetElement) {
        if (!this.isHidden && !this.container.nativeElement.contains(targetElement) && targetElement != this.control) {
            this.cancel();
        }
    }

    save(event?: MouseEvent) {
        this.onsave.emit();
        this.isHidden = true;
        if (event)
            event.stopPropagation();
    }
    cancel(event?: MouseEvent) {
        this.isHidden = true;
        this.oncancel.emit();
        if (event)
            event.stopPropagation();
    }

    show() {
        var rect = this._domAdapter.getBoundingClientRect(this.target);
        this.renderer.setElementStyle(this.container.nativeElement, 'left', rect.width - 30  /* outstand a bit */ + 'px')
        this.renderer.setElementStyle(this.container.nativeElement, 'top', -10 + 'px');
        this.isHidden = false;
    }

    @Output()
    onsave: EventEmitter<any> = new EventEmitter();
    @Output()
    oncancel: EventEmitter<any> = new EventEmitter();
}
