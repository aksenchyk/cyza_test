import { PopupComponent } from './popuppableEdit';
import { InertLink } from './inertLink';

export const SHARED_DIRECTIVES = [
     PopupComponent, InertLink
]