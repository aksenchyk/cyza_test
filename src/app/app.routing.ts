/* tslint:disable: max-line-length */
import { Routes } from '@angular/router';
import { NotFound404Component } from './404Not_found.component';

export const routes: Routes = [
    { path: '', redirectTo: 'auth', pathMatch: 'full' },
    { path: 'auth', loadChildren: './features/+auth/index#AuthModule', pathMatch: 'full' },
    { path: 'profile', loadChildren: './features/+profile/index#ProfileModule' },
    { path: '**', component: NotFound404Component }
];
